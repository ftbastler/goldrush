package de.ftbastler.goldrush.timers;

import org.bukkit.scheduler.BukkitRunnable;

import de.ftbastler.goldrush.enums.LobbyState;
import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.utilities.Lobby;

public class LobbyTimer {

	BukkitRunnable task;
	
	public LobbyTimer() {
		task = new BukkitRunnable() {
			
			@Override
			public void run() {
				Lobby lobby = GoldRush.getLobby();
				if(lobby.getLobbyState() == LobbyState.IDLE) {
					if(lobby.getGamers().size() >= 2) {
						if(lobby.getCountdown() > 0)
							lobby.setCountdown(lobby.getCountdown() - 1);
						else
							lobby.startGame();
					}
				}
				
				lobby.updateScoreboard();
			}
		};
	}
	
	public void start() {
		task.runTaskTimer(GoldRush.getInstance(), 0, 20);
	}
	
	public void stop() {
		task.cancel();
	}

}
