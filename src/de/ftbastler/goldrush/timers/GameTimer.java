package de.ftbastler.goldrush.timers;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.scheduler.BukkitRunnable;

import de.ftbastler.goldrush.libraries.ParticleEffects;
import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.utilities.Cuboid;
import de.ftbastler.goldrush.utilities.Game;
import de.ftbastler.goldrush.utilities.Cuboid.CuboidDirection;
import de.ftbastler.goldrush.utilities.Gamer;

public class GameTimer {

	BukkitRunnable task;
	Game game;

	public GameTimer(final Game currentGame) {
		game = currentGame;
		task = new BukkitRunnable() {
			Random r = new Random();
			
			@Override
			public void run() {				
				for(Cuboid c : game.getMap().getGoldSpawns()) {
					for(Block b : c.getFace(CuboidDirection.Up).getBlocks()) {
						if(r.nextInt(50) == 0) {
							if(b.getWorld().getHighestBlockAt(b.getLocation()).getType() == Material.GOLD_ORE ||
									b.getWorld().getHighestBlockAt(b.getLocation()).isLiquid() ||
									GoldRush.getLobby().getCurrentGame().getGoldBlocks().size() > 20)
								continue;
							
							Entity ent = b.getWorld().spawnFallingBlock(b.getLocation(), Material.GOLD_ORE, (byte) 0);
							((FallingBlock) ent).setDropItem(false);
							ent.setFallDistance(0);
							ParticleEffects.sendToLocation(ParticleEffects.MAGIC_CRITICAL_HIT, b.getLocation(), 1, 0, 1, 1, 100);
						}
					}
				}
				
				for(Cuboid c : game.getMap().getDropZones()) {
					for(Gamer g : game.getGamers()) {
						if(g.getGoldBlocks() > 0 && c.contains(g.getPlayer().getLocation())) {
							int i = g.getGoldBlocks();
							g.setGoldBlocks(0);
							g.setSaveGolds(g.getSaveGolds() + i);
							ParticleEffects.sendToLocation(ParticleEffects.FIREWORK_SPARK, g.getPlayer().getLocation(), 0, 1, 0, 1, 100);
							g.getPlayer().sendMessage(ChatColor.GOLD + "Added " + i + " golds to your bank. You now have " + g.getSaveGolds() + " golds on your bank account.");
						}
					}
				}
				
				game.setCountdown(game.getCountdown()-1);
				game.checkEnd();
				game.updateScoreboard();
			}
		};
	}

	public GameTimer start() {
		task.runTaskTimer(GoldRush.getInstance(), 0, 20);
		return this;
	}

	public GameTimer stop() {
		task.cancel();
		return this;
	}

}
