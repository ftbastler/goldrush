package de.ftbastler.goldrush.main;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.ftbastler.goldrush.commands.GameCommands;
import de.ftbastler.goldrush.events.GameListener;
import de.ftbastler.goldrush.events.PlayerListener;
import de.ftbastler.goldrush.events.PluginListener;
import de.ftbastler.goldrush.utilities.Cuboid;
import de.ftbastler.goldrush.utilities.Gamer;
import de.ftbastler.goldrush.utilities.Lobby;
import de.ftbastler.goldrush.utilities.GoldRushMap;
import de.ftbastler.goldrush.utilities.SerializableLocation;

public class GoldRush extends JavaPlugin {

	private static GoldRush instance;
	private static Logger logger;
	private static Lobby lobby;
	
	private File mapsFolder = new File(getPluginFolder(), "maps");
	private ArrayList<GoldRushMap> maps = new ArrayList<GoldRushMap>();
	private HashMap<String, Location> selection1 = new HashMap<String, Location>();
	private HashMap<String, Location> selection2 = new HashMap<String, Location>();
	
	public static GoldRush getInstance() {
		return instance;
	}
	
	public static Logger getPluginLogger() {
		return logger;
	}
	
	public static Lobby getLobby() {
		return lobby;
	}
	
	public ArrayList<GoldRushMap> getMaps() {
		return this.maps;
	}
	
	public GoldRushMap getMapByWorldName(String s) {
		GoldRushMap map = null;
		for(GoldRushMap xmap : GoldRush.getInstance().getMaps()) {
			if(xmap.getWorld().getName().equals(s)) {
				map = xmap;
			}
		}
		return map;
	}
	
	public GoldRushMap getMapByName(String s) {
		GoldRushMap map = null;
		for(GoldRushMap xmap : GoldRush.getInstance().getMaps()) {
			if(xmap.getMapName().equalsIgnoreCase(s)) {
				map = xmap;
			}
		}
		return map;
	}
	
	public Location getSelection1(String playerName) {
		return selection1.get(playerName);
	}

	public void setSelection1(String playerName, Location selection1) {
		this.selection1.put(playerName, selection1);
	}

	public Location getSelection2(String playerName) {
		return selection2.get(playerName);
	}

	public void setSelection2(String playerName, Location selection2) {
		this.selection2.put(playerName, selection2);
	}
	
	public File getPluginFolder() {
		return new File(getDataFolder(), "plugins/GoldRush/");
	}
	
	public void sendPluginMessage(String cmd, String args) {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF(cmd);
            out.writeUTF(args);
        } catch (Exception e) {
        	e.printStackTrace();
        }

        getServer().sendPluginMessage(this, "GoldRush", b.toByteArray());
	}
	
	@Override
	public void onLoad() {
		instance = this;
		logger = getLogger();
		
		ConfigurationSerialization.registerClass(Cuboid.class, "Cuboid");
		ConfigurationSerialization.registerClass(SerializableLocation.class, "Location");
	}
	
	@Override
	public void onEnable() {
		//Create config and maps folder
		saveDefaultConfig();
		if(!mapsFolder.exists() || !mapsFolder.isDirectory()) {
			mapsFolder.mkdirs();
		}
		
		//Check if the config has all required information
		Boolean sqlSet = true;
		for(String s : getConfig().getConfigurationSection("mysql").getKeys(false)) {
			if(getConfig().getConfigurationSection("mysql").get(s) == null || getConfig().getConfigurationSection("mysql").get(s) == "")
				sqlSet = false;
		}
		
		//If not, then print an error
		if(!sqlSet) {
			getPluginLogger().warning("You didn't set any MySQL login information in the configuration file.");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		if(!loadMaps()) {
			getPluginLogger().warning("You didn't set any maps. :/");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "GoldRush");
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "GoldRush", new PluginListener());
		
		registerCommands();
		registerEvents();
		
		sendPluginMessage("started", getConfig().getString("servername"));
		
		lobby = new Lobby();
		
		//This fixes errors when we reload the plugin
		for(Player player : Bukkit.getServer().getOnlinePlayers()) {
			lobby.addGamer(new Gamer(player));
			
			if(player != null && player.isOnline())
				player.teleport(GoldRush.getLobby().getSpawn());
			
			player.sendMessage("GoldRush has been reloaded.");
		}
	}
	
	@Override
	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
		
		if(GoldRush.getLobby().getCurrentGame() != null)
			GoldRush.getLobby().getCurrentGame().end();
	}
	
	private void registerCommands() {
		getCommand("goldrush").setExecutor(new GameCommands());
	}
	
	private void registerEvents() {
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		Bukkit.getPluginManager().registerEvents(new GameListener(), this);
	}

	private Boolean loadMaps() {
		try {	
			getPluginLogger().info("Loading maps from '" + mapsFolder.getPath() + "'...");
			
			if(!mapsFolder.isDirectory()) {
				getPluginLogger().info("Maps folder is not a directory.");
				return false;
			}
			
			for(File mapFile : mapsFolder.listFiles()) {
				getPluginLogger().info("Loading map '" + mapFile.getName() + "'...");
				
				if(!mapFile.isDirectory() || !mapFile.canRead()) {
					getPluginLogger().info("Map file is not a directory '" + mapFile.getPath() + "'.");
					continue;
				}
				
				File mapConfigFile = new File(mapFile, "map.yml");
				if(!mapConfigFile.exists() || !mapConfigFile.canRead()) {
					getPluginLogger().info("Map config file doesn't exits for '" + mapConfigFile.getPath() + "'.");
					continue;
				}
					
				YamlConfiguration mapConfig = YamlConfiguration.loadConfiguration(mapConfigFile);
				if(mapConfig == null || !mapConfig.isSet("name") || !mapConfig.isSet("author")) {
					getPluginLogger().info("Map config file has errors for '" + mapConfigFile.getPath() + "'.");
					continue;
				}
				
				/*
				ArrayList<Cuboid> goldSpawns = new ArrayList<Cuboid>();
				if(mapConfig.isSet("dropZones")) {
					ConfigurationSection cs = mapConfig.getConfigurationSection("dropZones");
					for(String s : cs.getKeys(false)) {
						Cuboid c = (Cuboid) cs.get(s);
						getPluginLogger().info("Loaded cuboid '" + c.toString() + "'.");
						goldSpawns.add(c);
					}
				}
				*/
				
				if(!mapConfig.getValues(true).containsKey("worldPath")) {
					mapConfig.set("worldPath", mapFile.getPath());
					mapConfig.save(mapConfigFile);
				}
				getMaps().add(new GoldRushMap(mapConfig.getValues(true)));
			}
			
			if(getMaps().size() == 0) {
				getPluginLogger().info("No maps have been found.");
				return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
