package de.ftbastler.goldrush.events;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import de.ftbastler.goldrush.main.GoldRush;

public class PluginListener implements PluginMessageListener {

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		GoldRush.getPluginLogger().info("Received message. Channel: " + channel + " Player: " + player + " Message: " + message.toString());
	}
	
}
