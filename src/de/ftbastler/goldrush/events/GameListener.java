package de.ftbastler.goldrush.events;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;

import de.ftbastler.goldrush.main.GoldRush;

public class GameListener implements Listener {

	@EventHandler
	public void a(EntityChangeBlockEvent event) {
		if(event.getEntityType() == EntityType.FALLING_BLOCK && event.getTo() == Material.GOLD_ORE) {
			GoldRush.getLobby().getCurrentGame().addGoldBlock(event.getBlock());
		}
	}
	
}
