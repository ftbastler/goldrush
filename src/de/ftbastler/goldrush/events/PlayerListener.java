package de.ftbastler.goldrush.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.utilities.Gamer;

public class PlayerListener implements Listener {

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if(!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
			event.setCancelled(true);
			return;
		}
			
		Gamer gamer = GoldRush.getLobby().getGamer((Player) event.getEntity());
		Gamer damager = GoldRush.getLobby().getGamer((Player) event.getDamager());
		
		if(gamer == null || damager == null || !gamer.isPlaying() || !damager.isPlaying())
			return;
		
		gamer.spillOutBlocks();
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if(!(event.getEntity() instanceof Player))
			return;
		
		Gamer gamer = GoldRush.getLobby().getGamer((Player) event.getEntity());
		
		if(gamer == null)
			return;
		
		event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		Gamer gamer = GoldRush.getLobby().getGamer(event.getPlayer());
		
		if(gamer == null || (gamer.isAdmin() && !gamer.isPlaying()))
			return;
		
		event.setCancelled(true);
		gamer.getPlayer().updateInventory();
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Gamer gamer = GoldRush.getLobby().getGamer(event.getPlayer());
		
		if(gamer == null || (gamer.isAdmin() && !gamer.isPlaying()))
			return;
		
		event.setCancelled(true);
		gamer.getPlayer().updateInventory();
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		GoldRush.getLobby().addGamer(new Gamer(event.getPlayer()));
		event.setJoinMessage(ChatColor.AQUA + " + "
				+ event.getPlayer().getDisplayName() + ChatColor.RESET + "" + ChatColor.AQUA  + " joined GoldRush.");
		
		final Player player = event.getPlayer();
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(player != null && player.isOnline())
					player.teleport(GoldRush.getLobby().getSpawn());
			}
		}.runTaskLater(GoldRush.getInstance(), 10);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		GoldRush.getLobby().removeGamer(
				GoldRush.getLobby().getGamer(event.getPlayer()));
		event.setQuitMessage(ChatColor.AQUA + " - "
				+ event.getPlayer().getDisplayName() + ChatColor.RESET + "" + ChatColor.AQUA + " left GoldRush.");
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (GoldRush.getLobby().getGamer(event.getPlayer()).getFrozen())
			event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Gamer gamer = GoldRush.getLobby().getGamer(event.getPlayer());
		if(gamer != null && gamer.isPlaying() && event.getClickedBlock() != null) {
			if(event.getClickedBlock().getType() == Material.GOLD_ORE && GoldRush.getLobby().getCurrentGame().isGoldBlock(event.getClickedBlock()) && (event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
				if(GoldRush.getLobby().getGamer(event.getPlayer()).getGoldBlocks() >= 5) {
					gamer.getPlayer().sendMessage(ChatColor.RED + "You can only carry a maximum of 5 golds.");
					return;
				}
				
				event.getClickedBlock().setType(Material.AIR);
				GoldRush.getLobby().getCurrentGame().remGoldBlock(event.getClickedBlock());
				gamer.pickedupGold(event.getClickedBlock());
			}
		}
		
		//Block selection
		if (event.getPlayer().hasPermission("gr.admin") && event.getPlayer().getItemInHand().getType() == Material.IRON_SPADE && event.getClickedBlock() != null) {
			if(event.getAction() == Action.LEFT_CLICK_BLOCK) {
				GoldRush.getInstance().setSelection1(event.getPlayer().getName(), event.getClickedBlock().getLocation());
				event.getPlayer().sendMessage(ChatColor.GRAY + "First postion set.");
			} else if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
				GoldRush.getInstance().setSelection2(event.getPlayer().getName(), event.getClickedBlock().getLocation());
				event.getPlayer().sendMessage(ChatColor.GRAY + "Second postion set.");
			}
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		event.setCancelled(true);
		event.getItem().remove();
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		event.setCancelled(true);
	}

}
