package de.ftbastler.goldrush.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.utilities.Cuboid;
import de.ftbastler.goldrush.utilities.GoldRushMap;

public class GameCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		Player player = null;
		if(sender instanceof Player) {
			player = (Player) sender;
		}
		
		if(args.length < 1) {
			sender.sendMessage("Usage: /goldrush <args>");
			return true;
		}
		
		if(args[0].equalsIgnoreCase("start")) {
			GoldRush.getLobby().startGame();
			return true;
		}
		
		//ADMIN COMMANDS FROM NOW ON
		if(player != null && !(player.hasPermission("gr.admin") || player.isOp())) {
			sender.sendMessage(ChatColor.RED + "You don't have permission to access this command.");
			return true;
		}
		
		if(args[0].equalsIgnoreCase("map")) {
			if(player == null)
				return false;
			
			if(args.length <= 1) {
				sender.sendMessage("Usage: /goldrush map <edit|goldSpawn|dropZone|list|goto>");
				return true;
			}
			
			if(args[1].equalsIgnoreCase("goldSpawn")) {
				if(GoldRush.getInstance().getSelection1(player.getName()) == null || GoldRush.getInstance().getSelection2(player.getName()) == null) {
					player.sendMessage(ChatColor.RED + "That's not a cuboid area! Please select 2 blocks.");
					return true;
				}
				
				GoldRushMap map = GoldRush.getInstance().getMapByWorldName(player.getWorld().getName());
				
				if(map == null) {
					player.sendMessage(ChatColor.RED + "The world you are in is not a map!");
					return true;
				}
				
				if(GoldRush.getInstance().getSelection1(player.getName()).getWorld().getName() != GoldRush.getInstance().getSelection2(player.getName()).getWorld().getName() || 
						GoldRush.getInstance().getSelection1(player.getName()).getWorld().getName() != map.getWorld().getName() || 
						GoldRush.getInstance().getSelection2(player.getName()).getWorld().getName() != map.getWorld().getName()) {
					player.sendMessage(ChatColor.RED + "The selected blocks are not in the same world!");
					return true;
				}
				
				if(map.addGoldSpawnZone(new Cuboid(GoldRush.getInstance().getSelection1(player.getName()), GoldRush.getInstance().getSelection2(player.getName())))) {
					GoldRush.getInstance().setSelection1(player.getName(), null);
					GoldRush.getInstance().setSelection2(player.getName(), null);
					player.sendMessage(ChatColor.GREEN + "Gold spawn successfullly added.");
				} else {
					player.sendMessage(ChatColor.RED + "Error while trying to set new gold spawn... Check console.");
				}
				return true;
			}
			
			if(args[1].equalsIgnoreCase("dropZone")) {
				if(GoldRush.getInstance().getSelection1(player.getName()) == null || GoldRush.getInstance().getSelection2(player.getName()) == null) {
					player.sendMessage(ChatColor.RED + "That's not a cuboid area! Please select 2 blocks.");
					return true;
				}
				
				GoldRushMap map = GoldRush.getInstance().getMapByWorldName(player.getWorld().getName());
				
				if(map == null) {
					player.sendMessage(ChatColor.RED + "The world you are in is not a map!");
					return true;
				}
				
				if(GoldRush.getInstance().getSelection1(player.getName()).getWorld().getName() != GoldRush.getInstance().getSelection2(player.getName()).getWorld().getName() || 
						GoldRush.getInstance().getSelection1(player.getName()).getWorld().getName() != map.getWorld().getName() || 
						GoldRush.getInstance().getSelection2(player.getName()).getWorld().getName() != map.getWorld().getName()) {
					player.sendMessage(ChatColor.RED + "The selected blocks are not in the same world!");
					return true;
				}
				
				if(map.addDropZone(new Cuboid(GoldRush.getInstance().getSelection1(player.getName()), GoldRush.getInstance().getSelection2(player.getName())))) {
					GoldRush.getInstance().setSelection1(player.getName(), null);
					GoldRush.getInstance().setSelection2(player.getName(), null);
					player.sendMessage(ChatColor.GREEN + "Drop zone successfullly added.");
				} else {
					player.sendMessage(ChatColor.RED + "Error while trying to set new drop zone... Check console.");
				}
				return true;
			}
			
			if(args[1].equalsIgnoreCase("spawn")) {
				GoldRushMap map = GoldRush.getInstance().getMapByWorldName(player.getWorld().getName());
				
				if(map == null) {
					player.sendMessage(ChatColor.RED + "The world you are in is not a map!");
					return true;
				}
				
				map.setSpawnpoint(player.getLocation());
				player.sendMessage(ChatColor.GREEN + "Spawnpoint successfullly set to your current location.");
				return true;
			}
			
			if(args[1].equalsIgnoreCase("tools")) {
				player.getInventory().addItem(new ItemStack(Material.IRON_SPADE, 1));
				sender.sendMessage(ChatColor.GREEN + "Right/left click with this spade to select a region for spawning gold. Use \"/goldrush map\" to save the selected region. You can repeat these steps as often as you want.");
				return true;
			}
			
			if(args[1].equalsIgnoreCase("list")) {
				String mapsList = null;
				for(GoldRushMap map : GoldRush.getInstance().getMaps()) {
					if(mapsList == null) {
						mapsList = map.getMapName();
					} else {
						mapsList += ", " + map.getMapName();
					}
				}
				
				sender.sendMessage(ChatColor.GREEN + "Maps: " + (mapsList == null ? "none" : mapsList));
				return true;
			}
			
			if(args[1].equalsIgnoreCase("goto")) {
				if(args.length != 3) {
					sender.sendMessage(ChatColor.RED + "Usage: /goldrush map goto <mapName>");
					return true;
				}
				
				GoldRushMap map = GoldRush.getInstance().getMapByName(args[2]);
				
				if(map == null) {
					player.sendMessage(ChatColor.RED + "That map doesn't exist. Use \"/goldrush map list\" for a list of available maps.");
					return true;
				}
				
				player.teleport(map.getSpawn());
				sender.sendMessage(ChatColor.GREEN + "Teleported to " + map.getMapName());
				return true;
			}
			
			player.sendMessage(ChatColor.RED + "Usage: /goldrush map <args>");
			return true;
		}
		
		return true;
	}

}
