package de.ftbastler.goldrush.enums;

public enum GameState {

	PREGAME, RUNNING, ENDED;
	
}
