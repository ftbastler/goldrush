package de.ftbastler.goldrush.utilities;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import de.ftbastler.goldrush.libraries.ParticleEffects;

public class Gamer {

	private String playerName;
	private Boolean isPlaying;
	private Boolean isFrozen;
	private int goldBlocks;
	private int saveGolds;

	public Gamer(Player player) {
		this.playerName = player.getName();
		resetPlayer();
	}

	public Player getPlayer() {
		return Bukkit.getServer().getPlayerExact(playerName);
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public void setPlaying(Boolean playing) {
		this.isPlaying = playing;
	}

	public Boolean isPlaying() {
		return this.isPlaying;
	}

	public Boolean isAdmin() {
		return getPlayer().hasPermission("gr.admin") || getPlayer().isOp();
	}
	
	public Boolean getFrozen() {
		return isFrozen;
	}

	public void setFrozen(Boolean isFrozen) {
		this.isFrozen = isFrozen;
	}

	public void setGoldBlocks(int i) {
		goldBlocks = i;
		update();
	}

	public void addGoldBlock() {
		goldBlocks += 1;
		update();
	}

	public int getGoldBlocks() {
		return goldBlocks;
	}

	@SuppressWarnings("deprecation")
	public void update() {
		if(getGoldBlocks() == 5)
			getPlayer().getWorld().strikeLightning(getPlayer().getLocation());
		
		getPlayer().getInventory().clear();
		getPlayer().setExp(0);
		
		getPlayer().setLevel(getSaveGolds());
		
		if(getGoldBlocks() > 0) {
			getPlayer().getInventory().addItem(new ItemStack(Material.GOLD_ORE, getGoldBlocks()));
			getPlayer().setExp((float) getGoldBlocks()/5);
		}
		
		switch(getGoldBlocks()) {
		case 0:
		case 1:
			getPlayer().removePotionEffect(PotionEffectType.SLOW);
			break;
		case 3:
		case 2:
			getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*60*60, 0), true);
			break;
		case 4:
			getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*60*60, 1), true);
			break;
		case 5:
			getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20*60*60, 2), true);
			break;
		default:
			getPlayer().removePotionEffect(PotionEffectType.SLOW);
	}
		
		getPlayer().updateInventory();
	}

	public void resetPlayer() {
		getPlayer().closeInventory();
		getPlayer().setHealth(20.0);
		getPlayer().getEnderChest().clear();
		getPlayer().setFireTicks(0);
		getPlayer().setExp(0);
		getPlayer().setLevel(0);
		getPlayer().setTotalExperience(0);
		getPlayer().setGameMode(GameMode.ADVENTURE);
		if (getPlayer().isInsideVehicle())
			getPlayer().getVehicle().eject();
		getPlayer().setFoodLevel(20);
		getPlayer().setExhaustion(20);
		getPlayer().setSaturation(20);
		getPlayer().setAllowFlight(false);
		getPlayer().getInventory().clear();
		getPlayer().getInventory().setArmorContents(new ItemStack[] {});
		for (PotionEffect potionEffect : getPlayer().getActivePotionEffects())
			getPlayer().removePotionEffect(potionEffect.getType());

		this.isPlaying = false;
		this.isFrozen = false;
		this.goldBlocks = 0;
		this.saveGolds = 0;
	}

	public void pickedupGold(Block b) {
		getPlayer().playSound(b.getLocation(), Sound.ORB_PICKUP, 1, 1);
		addGoldBlock();
	}

	public void spillOutBlocks() {
		if (getGoldBlocks() > 0) {
			ParticleEffects.sendToLocation(ParticleEffects.LAVA_SPARK, getPlayer().getLocation(), 0, 1, 0, 1, 50);
			getPlayer().sendMessage(ChatColor.RED + "You just lost your golds...");
			
			Random r = new Random();
			for (int i = 0; i <= getGoldBlocks(); i++) {
				int rix = r.nextBoolean() ? -1 : 1;
				int riz = r.nextBoolean() ? -1 : 1;

				Entity ent = getPlayer()
						.getLocation()
						.getWorld()
						.spawnFallingBlock(getPlayer().getLocation(),
								Material.GOLD_ORE, (byte) 0);
				((FallingBlock) ent).setDropItem(false);

				ent.setFallDistance(0);
				ent.setVelocity(new Vector(r.nextBoolean() ? (rix * (0.25D + (r
						.nextInt(3) / 5))) : 0.0D,
						0.6D + (r.nextInt(2) / 4.5D),
						r.nextBoolean() ? (riz * (0.25D + (r.nextInt(3) / 5)))
								: 0.0D));
			}
			setGoldBlocks(0);
		}
	}

	public int getSaveGolds() {
		return saveGolds;
	}

	public void setSaveGolds(int saveGolds) {
		this.saveGolds = saveGolds;
		update();
	}

}
