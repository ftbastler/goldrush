package de.ftbastler.goldrush.utilities;

import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import de.ftbastler.goldrush.enums.LobbyState;
import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.timers.LobbyTimer;

public class Lobby {

	ArrayList<Gamer> gamers = new ArrayList<Gamer>();
	Location spawnLocation;
	Game game;
	Scoreboard board;
	private final String OBJECTIVE_HUD = "GR_HUD";
	private Integer countdown = 0;
	private LobbyState lobbyState;
	
	public Lobby() {
		spawnLocation = Bukkit.getServer().getWorlds().get(0).getHighestBlockAt(Bukkit.getServer().getWorlds().get(0).getSpawnLocation()).getLocation();
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		setCountdown(60);
		setLobbyState(LobbyState.IDLE);
		
		Objective o = board.registerNewObjective(OBJECTIVE_HUD, "dummy");
		o.setDisplayName(ChatColor.GOLD + "GoldRush");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		new LobbyTimer().start();
	}
	
	public void startGame() {
		GoldRushMap map = GoldRush.getInstance().getMaps().get(new Random().nextInt(GoldRush.getInstance().getMaps().size()));
		game = new Game(map, gamers);
		setLobbyState(LobbyState.PLAYING);
	}
	
	public void gameEnded() {
		setCountdown(60);
		setLobbyState(LobbyState.IDLE);
		game = null;
		
		for(Gamer g : getGamers()) {
			g.getPlayer().setScoreboard(board);
		}
	}
	
	public Location getSpawn() {
		if(!spawnLocation.getWorld().getChunkAt(spawnLocation).isLoaded())
			spawnLocation.getWorld().getChunkAt(spawnLocation).load();
		
		return this.spawnLocation;
	}
	
	public void addGamer(Gamer gamer) {
		getGamers().add(gamer);
		gamer.getPlayer().setScoreboard(board);
	}

	public void removeGamer(Gamer gamer) {
		getGamers().remove(gamer);
		if(getCurrentGame() != null)
			getCurrentGame().getGamers().remove(gamer);
	}
	
	public ArrayList<Gamer> getGamers() {
		return this.gamers;
	}
	
	public Game getCurrentGame() {
		return this.game;
	}
	
	public Gamer getGamerByName(String playerName) {
		for(Gamer g : getGamers()) {
			if(g.getPlayerName().equals(playerName))
				return g;
		}
		return null;
	}
	
	public Gamer getGamer(Player player) {
		return getGamerByName(player.getName());
	}
	
	public void updateScoreboard() {
		Objective o = board.getObjective(OBJECTIVE_HUD);
		o.getScore(Bukkit.getOfflinePlayer(ChatColor.YELLOW + "Players")).setScore(getGamers().size());
		o.getScore(Bukkit.getOfflinePlayer(ChatColor.YELLOW + "Timer")).setScore(getCountdown());
	}

	public Integer getCountdown() {
		return countdown;
	}

	public void setCountdown(Integer countdown) {
		this.countdown = countdown;
	}

	public LobbyState getLobbyState() {
		return lobbyState;
	}

	public void setLobbyState(LobbyState lobbyState) {
		this.lobbyState = lobbyState;
	}
	
}
