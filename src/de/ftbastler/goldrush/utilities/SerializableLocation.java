package de.ftbastler.goldrush.utilities;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class SerializableLocation implements ConfigurationSerializable {

	private double x, y, z;
	private String world;

	public SerializableLocation(Location loc) {
		x = loc.getX();
		y = loc.getY();
		z = loc.getZ();
		world = loc.getWorld().getName();
	}

	public SerializableLocation(Map<String, Object> map) {
		x = (double) map.get("x");
		y = (double) map.get("y");
		z = (double) map.get("z");
		world = (String) map.get("w");
	}

	public Location getLocation() {
		World w = Bukkit.getWorld(world);
		if (w == null)
			return null;
		Location toRet = new Location(w, x, y, z);
		return toRet;
	}

	@Override
	public Map<String, Object> serialize() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("x", this.x);
		map.put("y", this.y);
		map.put("z", this.z);
		map.put("w", this.world);
		return map;
	}

	public static SerializableLocation deserialize(Map<String, Object> map) {
		return new SerializableLocation(map);
	}
}
