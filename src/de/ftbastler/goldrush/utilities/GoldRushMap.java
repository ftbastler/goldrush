package de.ftbastler.goldrush.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import de.ftbastler.goldrush.main.GoldRush;

public class GoldRushMap {

	private File worldDirectory;
	private World world;
	private String mapName;
	private String author;
	private Location spawnLocation;
	private ArrayList<Cuboid> goldSpawns;
	private ArrayList<Cuboid> dropZones;
		
	public GoldRushMap(Map<String, Object> map) {
		this.goldSpawns = new ArrayList<Cuboid>();
		this.dropZones = new ArrayList<Cuboid>();
		
		this.mapName = (String) map.get("name");
		this.author = (String) map.get("author");
		this.worldDirectory = new File((String) map.get("worldPath"));
		if(map.containsKey("dropZones"))
			for(String s : ((MemorySection) map.get("dropZones")).getKeys(false)) {
				dropZones.add((Cuboid) ((MemorySection) map.get("dropZones")).get(s));
			}
		if(map.containsKey("goldSpawns"))
			for(String s : ((MemorySection) map.get("goldSpawns")).getKeys(false)) {
				goldSpawns.add((Cuboid) ((MemorySection) map.get("goldSpawns")).get(s));
			}
		this.world = new WorldCreator(worldDirectory.getPath()).createWorld();
		if(map.containsKey("spawn"))
			this.spawnLocation = ((SerializableLocation) map.get("spawn")).getLocation();
		else
			this.spawnLocation = world.getSpawnLocation();
	}	

	public ArrayList<Cuboid> getGoldSpawns() {
		return this.goldSpawns;
	}
	
	public ArrayList<Cuboid> getDropZones() {
		return this.dropZones;
	}
	
	public Location getSpawn() {
		Location loc = this.spawnLocation;
		while(loc.getBlock().getType() != Material.AIR) {
			loc = loc.add(0, 1, 0);
		}
		return loc;
	}

	public World getWorld() {
		return this.world;
	}

	public String getMapName() {
		return this.mapName;
	}
	
	public String getAuthor() {
		return this.author;
	}

	public Boolean addGoldSpawnZone(Cuboid c) {
		File mapConfigFile = new File(worldDirectory, "map.yml");
		if(!mapConfigFile.exists()) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't add a region for gold spawning, the config file didn't exist.");
			return false;
		}
		
		YamlConfiguration mapConfig = YamlConfiguration.loadConfiguration(mapConfigFile);
		if(mapConfig == null) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't add a region for gold spawning, the config file was broken.");
			return false;
		}
		
		if(!mapConfig.isSet("goldSpawns")) {
			mapConfig.createSection("goldSpawns");
		}
		
		int i = 0;
		String name = "zone" + i;
		while(mapConfig.getConfigurationSection("goldSpawns").isSet(name)) {
			i++;
			name = "zone" + i;
		}
		
		mapConfig.getConfigurationSection("goldSpawns").set(name, c);
		this.goldSpawns.add(c);
		try {
			mapConfig.save(mapConfigFile);
			GoldRush.getPluginLogger().warning("Saved new gold spawning area " + name + " for map " + getMapName() + ".");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public Boolean addDropZone(Cuboid c) {
		File mapConfigFile = new File(worldDirectory, "map.yml");
		if(!mapConfigFile.exists()) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't add a region for a drop zone, the config file didn't exist.");
			return false;
		}
		
		YamlConfiguration mapConfig = YamlConfiguration.loadConfiguration(mapConfigFile);
		if(mapConfig == null) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't add a region for a drop zone, the config file was broken.");
			return false;
		}
		
		if(!mapConfig.isSet("dropZones")) {
			mapConfig.createSection("dropZones");
		}
		
		int i = 0;
		String name = "zone" + i;
		while(mapConfig.getConfigurationSection("dropZones").isSet(name)) {
			i++;
			name = "zone" + i;
		}
		
		mapConfig.getConfigurationSection("dropZones").set(name, c);
		this.dropZones.add(c);
		try {
			mapConfig.save(mapConfigFile);
			GoldRush.getPluginLogger().warning("Saved new gold drop area " + name + " for map " + getMapName() + ".");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void setSpawnpoint(Location loc) {
		File mapConfigFile = new File(worldDirectory, "map.yml");
		if(!mapConfigFile.exists()) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't set the spawn point, the config file didn't exist.");
			return;
		}
		
		YamlConfiguration mapConfig = YamlConfiguration.loadConfiguration(mapConfigFile);
		if(mapConfig == null) {
			GoldRush.getPluginLogger().warning("Oh no! Couldn't set the spawn point, the config file was broken.");
			return;
		}
		
		mapConfig.set("spawn", new SerializableLocation(loc));
		this.spawnLocation = loc;
		try {
			mapConfig.save(mapConfigFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
