package de.ftbastler.goldrush.utilities;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import de.ftbastler.goldrush.main.GoldRush;
import de.ftbastler.goldrush.timers.GameTimer;

public class Game {

	private GoldRushMap map;
	private ArrayList<Gamer> gamers;
	private ArrayList<Block> goldBlocks;
	private GameTimer timer;
	private Scoreboard board;
	private final String OBJECTIVE_HUD =  "GR_GAME_HUD";
	private final String OBJECTIVE_TAG =  "GR_GAME_TAG";
	int countdown;

	public Game(GoldRushMap map, ArrayList<Gamer> gamers) {
		this.map = map;
		this.gamers = gamers;
		this.goldBlocks = new ArrayList<Block>();
		this.countdown = 60*2;
		this.board = Bukkit.getScoreboardManager().getNewScoreboard();
		
		Objective o = board.registerNewObjective(OBJECTIVE_HUD, "dummy");
		o.setDisplayName(ChatColor.GOLD + "GoldRush");
		o.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Objective o2 = board.registerNewObjective(OBJECTIVE_TAG, "dummy");
		o2.setDisplayName(ChatColor.GOLD + "Golds");
		o2.setDisplaySlot(DisplaySlot.BELOW_NAME);
				
		if(!getMap().getWorld().isChunkLoaded(getMap().getWorld().getChunkAt(getMap().getWorld().getSpawnLocation())))
			getMap().getWorld().getChunkAt(getMap().getWorld().getSpawnLocation()).load();
		
		for(Gamer g : getGamers()) {
			g.resetPlayer();
			g.getPlayer().setScoreboard(board);
			g.getPlayer().teleport(map.getSpawn());
			g.setFrozen(true);
			g.setPlaying(true);
		}
		
		Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "" + "Playing on map \"" + map.getMapName() + "\" by " + map.getAuthor() + ".");
		
		new BukkitRunnable() {
			int countdown = 3;
			
			@Override
			public void run() {
				if(countdown == 0) {
					start();
					this.cancel();
				} else {
					Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "Game begins in " + countdown + " seconds.");
					for(Gamer g : getGamers())
						g.getPlayer().playSound(g.getPlayer().getLocation(), Sound.ORB_PICKUP, 1, 0);
				}
				countdown--;
			}
		}.runTaskTimer(GoldRush.getInstance(), 20, 20);
	}

	public void start() {
		Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "RUSH FOR GOLD NOW!");
		
		for(Gamer g : getGamers()) {
			g.getPlayer().playSound(g.getPlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
			g.setFrozen(false);
		}
		
		timer = new GameTimer(this).start();
	}
	
	public void end() {
		Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "The game ended!");
		
		timer.stop();
		
		for(Gamer g : getGamers()) {
			g.getPlayer().playSound(g.getPlayer().getLocation(), Sound.ORB_PICKUP, 1, 1);
			g.resetPlayer();
			g.getPlayer().teleport(GoldRush.getLobby().getSpawn());
			g.setPlaying(false);
		}
		
		for(Block b : goldBlocks) {
			b.setType(Material.AIR);
		}
		
		GoldRush.getLobby().gameEnded();
	}
	
	public int getCountdown() {
		return countdown;
	}

	public void setCountdown(int countdown) {
		this.countdown = countdown;
	}
	
	public ArrayList<Gamer> getGamers() {
		return this.gamers;
	}
	
	public GoldRushMap getMap() {
		return this.map;
	}
	
	public void addGoldBlock(Block b) {
		goldBlocks.add(b);
	}
	
	public void remGoldBlock(Block b) {
		goldBlocks.remove(b);
	}
	
	public Boolean isGoldBlock(Block b) {
		return goldBlocks.contains(b);
	}
	
	public ArrayList<Block> getGoldBlocks() {
		return goldBlocks;
	}

	public void updateScoreboard() {
		Objective o = board.getObjective(OBJECTIVE_HUD);
		Objective o2 = board.getObjective(OBJECTIVE_TAG);
		for(Gamer g : getGamers()) {
			o.getScore(g.getPlayer()).setScore(g.getSaveGolds());
			o2.getScore(g.getPlayer()).setScore(g.getGoldBlocks());
		}
	}

	public void checkEnd() {
		if(getGamers().size() <= 1) {
			end();
			return;
		}
		
		if(getCountdown() <= 0) {
			Gamer winner = null;
			for(Gamer g : getGamers()) {
				if(winner == null) {
					winner = g;
					continue;
				}
				
				if(g.getSaveGolds() > winner.getSaveGolds())
					winner = g;
			}
			
			Bukkit.getServer().broadcastMessage(ChatColor.GOLD + "" + ChatColor.BOLD + winner.getPlayerName() + " won!");
			end();
		}
	}
}
