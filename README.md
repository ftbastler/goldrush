GoldRush is a new type of minigame in Minecraft. Originally developed for the Minebox server, but never made it out of the development stage. Currently abandoned.

## License ##

GoldRush by ftbastler is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).